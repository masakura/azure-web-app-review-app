#!/bin/bash

AAD_APP=($(az ad sp create-for-rbac -n gitlab-ci --skip-assignment --output tsv --query "[appId,password,tenant]"))

AZURE_USERNAME=${AAD_APP[0]}
AZURE_PASSWORD=${AAD_APP[1]}
AZURE_TENANT=${AAD_APP[2]}

echo Set GitLab CI Variables
echo https://gitlab.com/masakura/azure-web-app-review-app/-/settings/ci_cd
echo
echo AZURE_USERNAME=$AZURE_USERNAME
echo AZURE_PASSWORD=$AZURE_PASSWORD
echo AZURE_TENANT=$AZURE_TENANT

az role assignment create --assignee $AZURE_USERNAME --role Contributor --resource-group development