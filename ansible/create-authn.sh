#!/bin/sh

AAD_CLIENT_SECRET=`pwgen -1 40`
AZURE_RESOURCE_GROUP=development

az ad app create --display-name $AZURE_RESOURCE_GROUP \
                 --identifier-uris https://*.azurewebsites.net \
                 --reply-urls https://*.azurewebsites.net/.auth/login/aad/callback \
                 --required-resource-accesses @azure-ad-manifest.json \
                 --password $AAD_CLIENT_SECRET > /dev/null
AAD_CLIENT_ID=`az ad app list --display-name $AZURE_RESOURCE_GROUP | jq .[0].appId -r`

echo Set GitLab CI Variables
echo https://gitlab.com/masakura/azure-web-app-review-app/settings/ci_cd
echo
echo AAD_CLIENT_ID=$AAD_CLIENT_ID
echo AAD_CLIENT_SECRET=$AAD_CLIENT_SECRET

